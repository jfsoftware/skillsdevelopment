package com.rest.test.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.test.entitys.Employ;
import com.rest.test.repository.EmployDao;
import com.rest.test.service.EmployService;

@RestController
@RequestMapping("employees")
public class EmployController implements EmployService {

	@Autowired
	private EmployDao employdao;
	@Override
	@GetMapping
	public ResponseEntity<List<Employ>> list() {
		List<Employ> employlist = employdao.findAll();
		return ResponseEntity.ok(employlist);		
	}
	
	@Override
	@RequestMapping(value="{employ}")
	public ResponseEntity<Employ> employId(@PathVariable("employ") Integer employid) {
		Optional<Employ> optionalEmploy = employdao.findById(employid);
		if(optionalEmploy.isPresent()) {
			return ResponseEntity.ok(optionalEmploy.get());
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@Override
	@PostMapping
	public ResponseEntity<Employ> newEmploy(@RequestBody Employ employ) {
		Employ employNew = employdao.save(employ);
		return ResponseEntity.ok(employNew);
	}


	@Override
	@PutMapping
	public ResponseEntity<Employ>saveEmploy(@RequestBody Employ e) {
		Optional<Employ> optionalEmploy = employdao.findById(e.getId());
		if(optionalEmploy.isPresent()) {
			Employ upadateEmploy = optionalEmploy.get();
			upadateEmploy.setFullname(e.getFullname());
			employdao.save(upadateEmploy);
			return ResponseEntity.ok(upadateEmploy);
		}else {
			return ResponseEntity.noContent().build();
		}
	}

	@Override
	@DeleteMapping(value = "{employ}")
	public void delete(@PathVariable("employ")Integer e) {
		employdao.deleteById(e);
	}

}

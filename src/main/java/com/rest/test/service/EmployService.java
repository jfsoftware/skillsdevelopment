package com.rest.test.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.rest.test.entitys.Employ;

public interface EmployService {
	public ResponseEntity<List<Employ>>list();
	public ResponseEntity<Employ>employId(Integer employid);
	public ResponseEntity<Employ>newEmploy(Employ employ);
	public ResponseEntity<Employ>saveEmploy(Employ e);
	public void delete(Integer e);
}

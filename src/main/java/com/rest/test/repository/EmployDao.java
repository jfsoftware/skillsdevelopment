package com.rest.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rest.test.entitys.Employ;

public interface EmployDao extends JpaRepository<Employ, Integer>{

}
